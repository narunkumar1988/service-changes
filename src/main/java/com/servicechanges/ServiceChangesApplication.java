package com.servicechanges;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceChangesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceChangesApplication.class, args);
	}

}
