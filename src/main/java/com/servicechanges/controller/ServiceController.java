package com.servicechanges.controller;

import com.servicechanges.model.Inventory;
import com.servicechanges.service.UploadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
public class ServiceController {

    Logger log = LoggerFactory.getLogger(ServiceController.class);

    @Autowired
    UploadService service;

    @PostMapping(path = "/inventory/upload", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public DeferredResult<ResponseEntity<List<Inventory>>> fileUpload(@RequestParam(name = "file") MultipartFile file, @RequestParam("delimiter") String delimiter){
        String fileName = file.getOriginalFilename();
        log.info("fileName: {}, delimiter: {}",fileName,delimiter);

        DeferredResult<ResponseEntity<List<Inventory>>> result = new DeferredResult<>();
        try{
            String data = new String(file.getBytes());
            log.info("Data: \n {}",data);
            List<Inventory> inventories = service.processFile(file, delimiter);
            inventories.forEach(inv -> log.info("Inventory:{}",inv));
            result.setResult(new ResponseEntity<>(inventories,HttpStatus.OK));
        }catch(IOException e){
            log.error("Exception: ", e);
            result.setResult(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
        }
        return result;
    }
}
