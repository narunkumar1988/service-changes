package com.servicechanges.model;

import com.opencsv.bean.CsvBindByName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Inventory {

    String productId;

    String productName;

    String description;

    Long quantity;

    Double price;

}
