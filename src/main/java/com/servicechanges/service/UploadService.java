package com.servicechanges.service;

import com.servicechanges.model.Inventory;
import com.servicechanges.util.UploadHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class UploadService {

    @Autowired
    UploadHelper helper;

    public List<Inventory> processFile(MultipartFile file, String separator){

        return helper.convertFileToBean(file, separator);


    }
}
