package com.servicechanges.util;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.servicechanges.model.Inventory;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

@Component
public class UploadHelper {

    Logger logger = LoggerFactory.getLogger(UploadHelper.class);

    @Value("${csv.column.mapping}")
    String mapping;

    public List<Inventory> convertFileToBean(MultipartFile file, String separator){
        List<Inventory> inventories = null;
        if(file!=null && StringUtils.isNotBlank(separator)) {
            try (InputStreamReader reader = new InputStreamReader(file.getInputStream());) {
                ColumnPositionMappingStrategy cs = new ColumnPositionMappingStrategy();
                cs.setType(Inventory.class);
                String mappers[] = mapping.split(",");
                cs.setColumnMapping(mappers);

                CsvToBean<Inventory> csvToBean = new CsvToBeanBuilder<Inventory>(reader)
                        .withType(Inventory.class)
                        .withMappingStrategy(cs)
                        .withSkipLines(1)
                        .withQuoteChar(CSVWriter.DEFAULT_QUOTE_CHARACTER)
                        .withSeparator(separator.charAt(0))
                        .withIgnoreLeadingWhiteSpace(Boolean.TRUE)
                        .build();
                inventories = csvToBean.parse();
            } catch (IOException e) {
                logger.error("Conversion error", e);
            }
        }


        return inventories;

    }


}
