package com.servicechanges.util;

import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class Helper {

    public static MultipartFile getMultiPartFile(String fileName, String path) {
        try {
            MultipartFile file = new MockMultipartFile("Sample.csv", new FileInputStream(new File("src/test/resources/mocks/Sample.csv")));
            return file;
        } catch (IOException e) {
            throw new AssertionError();
        }
    }
}
