package com.servicechanges.util;

import com.servicechanges.model.Inventory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class UploadHelperTest {

    @Autowired
    UploadHelper helper;

    @Test
    public void convertTest1() {

        MultipartFile file = Helper.getMultiPartFile("Sample.csv", "src/test/resources/mocks/Sample.csv");
        List<Inventory> inventories = helper.convertFileToBean(file, ",");
        Assertions.assertNotNull(inventories);
        Assertions.assertEquals(2, inventories.size());
    }

    @Test
    public void convertTest2() {

        MultipartFile file = Helper.getMultiPartFile("Sample.csv", "src/test/resources/mocks/Sample2.csv");
        List<Inventory> inventories = helper.convertFileToBean(file, ",");
        Assertions.assertNotNull(inventories);
        Assertions.assertEquals(2, inventories.size());
    }


}
